# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
WebShop::Application.config.secret_key_base = 'ec41e62e019ce18d9634b390af4fc8559e5095116424dc0a43d6eedec24e5c362924d11a4e12665ce82bc0b835cc03753e18292b55faff1ae6934aa9b9cf9486'
